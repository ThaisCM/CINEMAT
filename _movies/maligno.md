---
title: "Maligno"
title_link: maligno
trailer: "https://www.youtube.com/embed/y2S9VzDDdM8"
vote_average: 5.9
popularity: 56.822
poster_path: "Maligno.jpg"
original_language: en
original_title: "The Prodigy"
genre:
- "Terror"
- "Thriller"
backdrop_path: "/oxTgovZ3TQ8LLxifUm16ROMULP5.jpg"
overview: "Una madre preocupada por el comportamiento perturbador de su hijo, está
  convencida de que algo sobrenatural está transformando la vida del pequeño y de
  quienes lo rodean, poniéndolos en peligro."
release_date: "2019-02-06"
duration: "92 min"
country: "Estados Unidos"
year: 2019
directors:
- "Nicholas McCarthy"
actors:
- "Taylor Schilling"
- "Brittany Allen"
- "Jackson Robert Scott"
- "Colm Feore"
---

{% include pelis.html %}
