---
title: "¡Shazam!"
title_link: shazam
trailer: "https://www.youtube.com/embed/B_DpkUSH2Mk"
vote_average: 7.2
popularity: 307.286
poster_path: "Shazam.jpg"
original_language: en
original_title: "Shazam!"
genre:
- "Acción"
- "Ciencia ficción"
backdrop_path: "/bi4jh0Kt0uuZGsGJoUUfqmbrjQg.jpg"
overview: "Todos llevamos un superhéroe dentro, solo se necesita un poco de magia
para sacarlo a la luz. Cuando Billy Batson, un niño de acogida de 14 años que
ha crecido en las calles, grita la palabra 'SHAZAM!' se convierte en el Superhéroe
adulto Shazam, por cortesía de un antiguo mago. Dentro de un cuerpo musculoso
y divino, Shazam esconde un corazón de niño. Pero lo mejor es que en esta versión
de adulto consigue realizar todo lo que le gustaría hacer a cualquier adolescente
con superpoderes: ¡divertirse con ellos! ¿Volar? ¿Tener visión de rayos X? ¿Disparar
un rayo con las manos? ¿Saltarse el examen de sociales? Shazam va a poner a prueba
los límites de sus habilidades con la inconsciencia propia de un niño. Pero necesitará
dominar rápidamente esos poderes para luchar contra las letales fuerzas del mal
que controla el Dr. Thaddeus Sivana."
release_date: "2019-03-23"
duration: "132 min"
country: "Estados Unidos"
year: 2019
directors:
- "David F. Sandberg"
actors:
- "Asher Angel"
- "Djimon Hounsou"
- "Grace Fulton"
- "Jack Dylan Grazer"
- "Mark Strong"
- "Zachary Levi"
---

{% include pelis.html %}
