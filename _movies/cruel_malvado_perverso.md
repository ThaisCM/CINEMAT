---
title: "Extremadamente cruel, malvado y perverso"
title_link: cruel_malvado_perverso
trailer: "https://www.youtube.com/embed/8ElIGWb81as"
vote_average: 7.1
popularity: 78.483
poster_path: "Cruel,Malvado.jpg"
original_language: en
original_title: "Extremely Wicked, Shockingly Evil and Vile"
genre:
- "Thriller"
- "Drama"
backdrop_path: "/5yXNf13ly6kFQSbI5BZRQlDhCib.jpg"
overview: "Ted Bundy fue uno de los asesinos en serie más peligroso de los años 70
  pero, además de asesino fue un secuestrador, violador, ladrón, necrófilo... Su
  novia, Elizabeth Kloepfer, se convirtió en una de sus más fieles defensoras, negándose
  a creer la verdad sobre él durante años. La historia de sus numerosos y terribles
  crimenes contada a través de los ojos de Elizabeth."
release_date: "2019-05-02"
duration: "108 min"
country: "Estados Unidos"
year: 2019
directors:
- "Joe Berlinger"
actors:
- "Zac Efron"
- "Lily Collins"
- "John Malkovich"
- "Kaya Scodelario"
- "Jim Parsons"
- "Haley Joel Osment"
- "Dylan Baker"
- "Grace Victoria Cox"
---

{% include pelis.html %}
