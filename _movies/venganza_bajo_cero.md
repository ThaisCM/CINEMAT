---
title: "Venganza bajo cero"
title_link: venganza_bajo_cero
trailer: "https://www.youtube.com/embed/oRFrcU47AbE"
vote_average: 5.2
popularity: 129.682
poster_path: "VenganzaBajoCero.jpg"
original_language: en
original_title: "Cold Pursuit"
genre:
- "Acción"
- "Suspense"
- "Drama"
backdrop_path: "/aiM3XxYE2JvW1vJ4AC6cI1RjAoT.jpg"
overview: "Un conductor de una barredora de nieve lleva una vida tranquila hasta
  que la muerte repentina de su hijo hace que se vea envuelto en una guerra entre
  narcotraficantes, armado solo con maquinaria pesada y la suerte del principiante.
  Remake de la película noruega 'Uno tras otro'."
release_date: "2019-02-07"
duration: "118 min"
country: "Gran Bretaña"
year: 2019
directors:
- "Hans Petter Moland"
actors:
- "Domenick Lombardozzi"
- "Emmy Rossum"
- "Laura Dern"
- "Liam Neeson"
- "Tom Bateman"
- "Tom Jackson"
---

{% include pelis.html %}
